angular.module('parkLife.controllers')
  .controller('HomeCtrl', ['$scope', '$timeout', 'parks', function ($scope, $timeout, parks) {

    $scope.state = {
      add: false,
      added: false
    };

    $scope.title = "Parklife";

    $scope.addPark = function addPark() {
      $scope.sortReverse = false;
      $scope.newPark.isNew = true;
      //$scope.newPark.rating = 4;
      $scope.parks.push(angular.copy($scope.newPark));
      $scope.newPark.title = '';
      $scope.newPark.description = '';
      $scope.state.add = false;
      $scope.state.added = true;

      $timeout(function () {
        $scope.state.added = false;
      }, 5000);
    };

    // get parks
    parks.get(function (data) {
      $scope.parks = data.groenzone;
    });

  }]);