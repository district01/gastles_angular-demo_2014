
// let's fetch our directives module and add a directive to it...
angular.module('parkLife.directives')
  // a simple park directive to list parks
  .directive('park', function() {
    return {
      templateUrl: '/05-combined/views/park.htm',
      restrict: 'AE',
      scope: {
        park: '=park'
      },
      link: function($scope, element, attrs, controller) {

      }
    };
  })
  // add a directive to rate parks...
  .directive('rating',
    [
      function RatingWrapper() {
        return {
          templateUrl: '/05-combined/views/rating.htm',
          restrict: 'AE',
          scope: {
            ngModel: '=',
            stars: '='
          },
          link: function link($scope, element, attrs, ctrl) {
            var rating = $scope.ngModel || 0,
              stars = $scope.stars || 5,
              starsList = [],
              updateStars,
              star,
              i;

            rating = parseInt(rating, 10);
            stars = parseInt(stars, 10);

            updateStars = function updateStars(val) {
              var nrOfStars = $scope.starsList.length;
              for (var i = 0; i < nrOfStars; i++) {
                var star = $scope.starsList[i];
                star.filled = (star.nr <= val);
              }
            };

            $scope.setRating = function setRating(val) {
              $scope.ngModel = val;
            };

            $scope.$watch('ngModel', function watchModel(newValue, oldValue, scope) {
              updateStars(newValue);
            });

            for (i = 0; i < stars; i += 1) {
              star = {
                nr: i + 1,
                filled: i < rating
              };
              starsList.push(star);
            }

            // make the list available to render
            $scope.starsList = starsList;

            element.on('mouseenter', '.star', function onMouseEnter(data) {
              var el = $(this);
              var prev = el.parents('li:first').prevAll();
              el.add(prev.find('.star')).addClass('hover');
            });
            element.on('mouseleave', '.star', function onMouseLeave(data) {
              $(this).parents('ul:first').find('.hover').removeClass('hover');
            });
          }
        };
      }
    ]
  );