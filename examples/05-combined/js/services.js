angular.module('parkLife.services')
  .factory('parks', ['$resource', function($resource) {
    return $resource('/resources/data.json',{ }, {
      getData: {method:'GET'}
    });
  }]);