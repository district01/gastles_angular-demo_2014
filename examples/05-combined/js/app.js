
// we create the modules so other parts of the app can use them.
angular.module('parkLife.services', ['ngResource']);
angular.module('parkLife.directives', []);
angular.module('parkLife.filters', []);
angular.module('parkLife.controllers', ['parkLife.services']);

// let's create the app module itself too...
angular.module('parkLife', ['parkLife.controllers', 'parkLife.directives', 'parkLife.filters', 'parkLife.services'])
  .run(function() {
    // do something when the app starts...
  });