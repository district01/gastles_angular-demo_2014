#Angular Demo


##Presentation

can be found in the folder `/presentation/angular/index.html`.

##Setup

What do you need to start with this project? Well you can either start it up on your own server (apache or http-server or ...) or you can use the included gruntfile.

To make it work with grunt, you will need NodeJS, you can find it at [nodeJS site](http://www.nodejs.org). After installing node you can install the dependencies for this project with the `npm install` command. Make sure you have the grunt command line interface `npm install grunt-cli -g`. Now you are ready and can just run the included webserver by typing the `grunt` command in the root of the app.